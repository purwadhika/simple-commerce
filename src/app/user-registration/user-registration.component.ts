import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-user-registration',
  templateUrl: './user-registration.component.html',
  styleUrls: ['./user-registration.component.css']
})
export class UserRegistrationComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  @Input("user") userList : Object[];

  newName : string = "";
  newAddress : string = "";
  newEmail : string = "";

  registerUser() {

    var id = 1;
    if (this.userList.length > 0) {
      id = this.userList[this.userList.length - 1]["id"] + 1;
    }

    this.userList.push({"id":id, "name":this.newName, "address":this.newAddress, "email":this.newEmail});

    this.newName = "";
    this.newAddress = "";
    this.newEmail = "";
    
  }

}
