import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class ApiService {

  constructor(private http:Http) { }

  getData(){
    return this.http
    .get('https://jsonplaceholder.typicode.com/usershh')
    .map(result => result.json() )
    .catch(error => Observable.throw(error.json().error) || "Server Error");
}

addData(obj: Object) {
  let body = JSON.stringify(obj);
  let headers = new Headers({ "Content-Type" : "application/json" });
  let options = new RequestOptions({ headers : headers });

  this.http.post('https://jsonplaceholder.typicode.com/users', body, options)
  .subscribe(result => console.log(result.json()));
  
}



}
