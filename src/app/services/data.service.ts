import { Injectable } from '@angular/core';

@Injectable()
export class DataService {

  cars = ['A' , 'B', 'C'];

  constructor() { }

  getTotal(orderList:Object[]):number{
    var total = 0;
    orderList.forEach(order => {
      total += (order["qty"] * order["price"]);
    });
    return total;
  }
}
