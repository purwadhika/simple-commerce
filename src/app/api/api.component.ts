import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api.service';

@Component({
  selector: 'app-api',
  templateUrl: './api.component.html',
  styleUrls: ['./api.component.css']
})
export class ApiComponent implements OnInit {

  data:object[];

  constructor(private api:ApiService) { }
 
 result(rs){

 }

  ngOnInit() {
    this.api.getData().subscribe( result => this.data = result );
  }

  addUser() {
    this.api.addData( { "name":"User 1" } );
  }

}
