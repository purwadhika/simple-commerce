import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {

  @Input('order') orderList: Object[];
  @Output() total: EventEmitter<number> = new EventEmitter<number>();

  constructor() { }

  ngOnInit() {
  }

  addOrder(productId){
    for (var i = 0; i < this.orderList.length; i++) {
      if (this.orderList[i]["productId"] == productId) {
        this.orderList[i]["qty"]++; 
        break;
      }
    }
    this.total.emit(this.getTotal());
  }

  removeOrder(productId){
    for (var i = 0; i < this.orderList.length; i++) {
      if (this.orderList[i]["productId"] == productId) {
        if (this.orderList[i]["qty"] > 1) {
          this.orderList[i]["qty"]--; 
        }
        else{
          this.orderList.splice(i, 1);
        }
        break;
      }
    }
    this.total.emit(this.getTotal());
  }

  getTotal():number{
    var total = 0;
    this.orderList.forEach(order => {
      total += (order["qty"] * order["price"]);
    });
    
    return total;
  }

}
