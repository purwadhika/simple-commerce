import { Component, OnInit } from '@angular/core';
import { DataService } from './services/data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  
  ngOnInit(){
    
  }

  userList : Object[] = [
    {"id":1, "name":"A", "address":"-", "email":"-"},
    {"id":2, "name":"B", "address":"-", "email":"-"},
  ];
  

  // productList : Object[] = [
  //   {"id":"1", "name":"Product 1", "price":1000, "img":"/assets/img/product.jpg", "desc":"This is sample product"},
  //   {"id":"2", "name":"Product 2", "price":2000, "img":"/assets/img/product.jpg", "desc":"This is sample product"},
  //   {"id":"3", "name":"Product 3", "price":3000, "img":"/assets/img/product.jpg", "desc":"This is sample product"},
  //   {"id":"4", "name":"Product 4", "price":4000, "img":"/assets/img/product.jpg", "desc":"This is sample product"}
  // ];
  // orderList : Object[] = [
  //   {"productId":"1", "productName":"Product 1", "price":1000, "qty":1},
  //   {"productId":"2", "productName":"Product 2", "price":2000, "qty":1},
  // ];

  // total : number = 0;

  // constructor(private dataService:DataService){}

  // setTotal(t : number){
  //   this.total = t;
  // }

}
