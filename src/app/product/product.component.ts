import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DataService } from '../services/data.service';


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

  @Input('product') productList: Object[];
  @Input('order') orderList: Object[];
  @Output() total: EventEmitter<number> = new EventEmitter<number>();

  constructor(private dataService:DataService) { }

  ngOnInit() {
  }

  buy(productId){
    var isExist = false;

    for (var i = 0; i < this.orderList.length; i++) {
      if (this.orderList[i]["productId"] == productId) {
        this.orderList[i]["qty"]++;
        isExist = true;
        break;
      }
    }

    if (!isExist) {
      var product;
      for (var i = 0; i < this.productList.length; i++) {
        if (this.productList[i]["id"] == productId) {
          product = this.productList[i];
          break
        }
      }
      this.orderList.push({"productId":product["id"], "productName":product["name"], "price":product["price"], "qty":1}); 
    }

    this.total.emit(this.dataService.getTotal(this.orderList));
    
  }

  // getTotal():number{
  //   var total = 0;
  //   this.orderList.forEach(
  //     order => {
  //        total += (order["qty"] * order["price"]);
  //     }
  //   );
    
  //   return total;
  // }

}
